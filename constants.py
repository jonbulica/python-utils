import os

# we assume that the utils folder is a subdirectory of you root project folder
ROOT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..')

CONFIG_DIR = os.path.join(ROOT_DIR, 'config')
MAIN_CONFIG = os.path.join(CONFIG_DIR, 'main.ini')
