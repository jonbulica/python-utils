__author__ = "jbu"
__version__ = 0.2

from configparser import ConfigParser

from utils.constants import MAIN_CONFIG as default_config


class Config:
    def __init__(self, file=default_config):
        self.file = file
        self.config = ConfigParser()
        self.config.read(file)

    def parse(self):
        return self.config

    def parse_section(self, section):
        if section in self.config.sections():
            return self.config[section]
        else:
            raise Exception("Section '{}' was not found. Is your settings file path correct?".format(section))
