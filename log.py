#!/usr/bin/python3
__author__ = "jbu"
__version__ = 0.5

import logging.config
import os
from sys import stdout

from utils.config import Config
from utils.constants import ROOT_DIR


class Logger:
    def __init__(self, name):
        conf = Config().parse_section("logging")
        log_level = Logger.parse_level(conf["level"])
        console_level = Logger.parse_level(conf["console"])
        # create logger
        logger = logging.getLogger(name)
        logger.setLevel(log_level)

        # create formatter
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - [%(filename)s:%(lineno)s - %(funcName)s()]: "
                                      "%(message)s")

        # create file handler
        fh = None
        if conf.get("directory") and conf.get("log_file"):
            fh = logging.FileHandler(os.path.join(ROOT_DIR, conf["directory"], conf["log_file"]))
            fh.setLevel(log_level)
            fh.setFormatter(formatter)

        # create console handler to log error in the console
        ch = logging.StreamHandler(stdout)
        ch.setLevel(console_level)
        ch.setFormatter(formatter)

        # add handlers to the logger
        if not logger.hasHandlers():
            for handler in [fh, ch]: 
                if handler: logger.addHandler(handler)
        self._logger = logger

    def get(self):
        return self._logger

    # noinspection PyProtectedMember
    @staticmethod
    def parse_level(conf_value):
        return logging._nameToLevel.get(conf_value)
