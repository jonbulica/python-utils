from functools import wraps

def notimplemented(func):
    def func_wrapper(*args, **kwargs):
        func(*args, **kwargs)
        raise NotImplementedError("You should override this method in the child implementation!")
    return func_wrapper


def disabled(func):
    def empty_func(*args,**kargs):
        pass
    return empty_func


def expect(exception_cls, msg):
    def except_decorator(func):
        @wraps(func)
        def func_wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception_cls as e:
                print(msg, ":", e)
        return func_wrapper
    return except_decorator
